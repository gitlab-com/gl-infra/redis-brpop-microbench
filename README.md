# redis-brpop-microbench

This is a small microbenchmark harness based on [this BRPOP load generator](https://github.com/redis/redis/issues/8668#issuecomment-804350847).

It starts a `redis-server`, captures a `perf` profile, then runs a synthetic `BRPOP` workload against the cluster, and finally spits out some statistics and a flamegraph.

Example statistics:

```
{
  "level": "info",
  "ts": "2021-04-06T08:30:31.335Z",
  "msg": "stats",
  "cpu_time_total": 79.519217,
  "cpu_time_sys": 16.592132,
  "cpu_time_user": 62.927085,
  "brpop_calls": "1000980",
  "brpop_usec": "25563833",
  "brpop_usec_per_call": "25.54",
  "lpush_calls": "1000000",
  "lpush_usec": "804367",
  "lpush_usec_per_call": "0.80"
}
```

## Setup

To create the VM in google cloud:

```
gcloud beta compute --project=iwiedler instances create redis-brpop-microbench --zone=us-east1-b --machine-type=e2-highcpu-16 --subnet=default --network-tier=PREMIUM --maintenance-policy=MIGRATE --image=ubuntu-2004-focal-v20210325 --image-project=ubuntu-os-cloud --boot-disk-size=10GB --boot-disk-type=pd-balanced --boot-disk-device-name=redis-brpop-microbench
```

To SSH into it:

```
gcloud --project iwiedler compute ssh redis-brpop-microbench
```

Then, follow the setup script from `Vagrantfile`.

And then, to install the microbench tool:

```
sudo git clone https://gitlab.com/gitlab-com/gl-infra/redis-brpop-microbench.git /opt/redis-brpop-microbench
(cd /opt/redis-brpop-microbench && sudo go build)
sudo ln -s /opt/redis-brpop-microbench/redis-brpop-microbench /usr/local/bin/redis-brpop-microbench
cp /opt/redis-brpop-microbench/redis.conf .

sudo sysctl vm.overcommit_memory=1
echo never | sudo tee /sys/kernel/mm/transparent_hugepage/enabled
ulimit -S -n 16384
```

## Run

Basic execution:

```
igor@redis-brpop-microbench:~$ redis-brpop-microbench

{"level":"info","ts":"2021-04-06T08:16:10.308Z","msg":"starting redis-server"}
{"level":"info","ts":"2021-04-06T08:16:10.329Z","msg":"starting consumers","num_consumers":1000,"brpop_timeout":1,"brpop_num_keys":100,"work_duration":0.005,"work_volatility":1}
{"level":"info","ts":"2021-04-06T08:16:10.340Z","msg":"starting producers","num_producers":100,"messages_per_producer":200,"message_bytes":128}
{"level":"info","ts":"2021-04-06T08:16:10.340Z","msg":"waiting on producers"}
{"level":"info","ts":"2021-04-06T08:16:12.121Z","msg":"monitoring queues"}
{"level":"info","ts":"2021-04-06T08:16:12.129Z","msg":"stats","cpu_time_total":1.626856,"cpu_time_sys":0.277689,"cpu_time_user":1.349167,"brpop_calls":"20997","brpop_usec":"532493","brpop_usec_per_call":"25.36","lpush_calls":"20000","lpush_usec":"17292","lpush_usec_per_call":"0.86"}
```

From there, it's possible to tune parameters:

```
igor@redis-brpop-microbench:~$ redis-brpop-microbench -messages-per-producer=10000 -consumer-brpop-num-keys=100  -consumers=1000 2>&1 | jq
```

## Design goals

* Self-contained
* Easy to set up
* Easy to tweak parameters
