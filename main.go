package main

import (
	"context"
	"encoding/base64"
	"flag"
	"fmt"
	"log"
	"math/rand"
	"os"
	"os/exec"
	"runtime"
	"strconv"
	"strings"
	"sync"
	"syscall"
	"time"

	"github.com/cenkalti/backoff/v4"
	"github.com/go-redis/redis/v8"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

// macos
// sudo sysctl -w kern.maxfiles=65000
// ulimit -S -n 16384

// linux
// sudo sysctl vm.overcommit_memory=1
// echo never | sudo tee /sys/kernel/mm/transparent_hugepage/enabled
// ulimit -S -n 16384

// pbpaste | jq -r '[.cpu_time, .brpop_calls, .brpop_usec, .brpop_usec_per_call, .lpush_calls, .lpush_usec, .lpush_usec_per_call, .ts]|join("\n")' | pbcopy

// based on https://github.com/redis/redis/pull/8689#issuecomment-806315828
//
// sudo perf probe -q -x $(which redis-server) --add blockForKeys
// sudo perf probe --list
// sudo perf stat -a -e probe_redis:* -x, -- sleep 3600 2>&1

// sudo bpftrace -e 'uprobe:/usr/local/bin/redis-server:blockForKeys { @ = count(); }'
// sudo bpftrace -e 'uprobe:/usr/local/bin/redis-server:blockForKeys { @[arg3] = count(); }'
// sudo bpftrace -e 'uprobe:/usr/local/bin/redis-server:unblockClient { @ = count(); }'
// sudo bpftrace -e 'uprobe:/usr/local/bin/redis-server:dictAdd { @[ustack()] = count(); } interval:s:1 { time("%H:%M:%S\n"); print(@); clear(@); }'

var (
	KeyPrefix = "queue:"
	Keys      = []string{}

	NumKeys = flag.Int("keys", 100, "")

	NumProducers         = flag.Int("producers", 100, "")
	MessagesPerProducer  = flag.Int("messages-per-producer", 200, "")
	ProducerMessageBytes = flag.Int("producer-message-bytes", 128, "")

	NumConsumers           = flag.Int("consumers", 1000, "")
	ConsumerBRPopTimeout   = flag.Duration("consumer-brpop-timeout", time.Second, "")
	ConsumerBRPopNumKeys   = flag.Int("consumer-brpop-num-keys", 100, "")
	ConsumerWorkDuration   = flag.Duration("consumer-work-duration", 5*time.Millisecond, "")
	ConsumerWorkVolatility = flag.Float64("consumer-work-volatility", 1.0, "factor by which work duration can vary")

	randomSeed = flag.Int64("random-seed", 42, "")

	logLevel = zap.LevelFlag("log-level", zap.InfoLevel, "debug|info|warn|error|dpanic|panic|fatal")
)

func redisServer(ctx context.Context) *exec.Cmd {
	cmd := exec.CommandContext(ctx, "redis-server", "redis.conf")
	if zap.L().Check(zap.DebugLevel, "") != nil {
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
	}
	err := cmd.Start()
	if err != nil {
		panic(err)
	}

	operation := func() error {
		rdb := redis.NewClient(&redis.Options{
			Addr: "127.0.0.1:6379",
		})
		return rdb.Ping(ctx).Err()
	}
	err = backoff.Retry(operation, backoff.NewConstantBackOff(time.Millisecond*100))
	if err != nil {
		panic(err)
	}

	return cmd
}

func redisInfoCPU(ctx context.Context, rdb *redis.Client) (time.Duration, time.Duration) {
	info := rdb.Info(ctx, "cpu")
	if info.Err() != nil {
		panic(info.Err())
	}

	redisSystemTime := 0.0
	redisUserTime := 0.0

	lines := strings.Split(info.Val(), "\r\n")
	for _, line := range lines {
		if strings.Contains(line, ":") {
			kv := strings.Split(line, ":")
			if len(kv) == 2 {
				switch kv[0] {
				case "used_cpu_sys":
					v, err := strconv.ParseFloat(kv[1], 64)
					if err != nil {
						panic(err)
					}
					redisSystemTime = v
				case "used_cpu_user":
					v, err := strconv.ParseFloat(kv[1], 64)
					if err != nil {
						panic(err)
					}
					redisUserTime = v
				}
			}
		}
	}

	return time.Duration(int(redisSystemTime*1000000)) * time.Microsecond, time.Duration(int(redisUserTime*1000000)) * time.Microsecond
}

func redisInfoCommandStats(ctx context.Context, rdb *redis.Client) map[string]interface{} {
	info := rdb.Info(ctx, "commandStats")
	if info.Err() != nil {
		panic(info.Err())
	}

	stats := make(map[string]interface{})

	lines := strings.Split(info.Val(), "\r\n")
	for _, line := range lines[1:] {
		kv := strings.Split(line, ":")
		if len(kv) == 2 {
			k := kv[0]
			pairs := strings.Split(kv[1], ",")
			for _, pair := range pairs {
				kv2 := strings.Split(pair, "=")
				if len(kv2) == 2 {
					k2 := kv2[0]
					v2 := kv2[1]
					stats[k+"_"+k2] = v2
				}
			}
		}
	}

	return stats
}

func redisConsumer(ctx context.Context) {
	rdb := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})

	n := *ConsumerBRPopNumKeys
	keys := make([]string, n)
	for i := 0; i < n; i++ {
		key := Keys[rand.Intn(len(Keys))]
		keys[i] = key
	}

	for {
		select {
		case <-ctx.Done():
			return
		default:
		}

		_, err := rdb.BRPop(ctx, *ConsumerBRPopTimeout, keys...).Result()
		if err == context.Canceled {
			return
		}
		if err == redis.Nil {
			continue
		}
		if err != nil {
			panic(err)
		}

		if ConsumerWorkDuration.Microseconds() > 0 {
			volatility := *ConsumerWorkVolatility
			jitter := int(ConsumerWorkDuration.Microseconds()) - rand.Intn(int((float64(ConsumerWorkDuration.Microseconds())*volatility)*2))
			time.Sleep(*ConsumerWorkDuration + time.Duration(jitter)*time.Microsecond)
		}
	}
}

func redisProducer(ctx context.Context) {
	rdb := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})

	for i := 0; i < *MessagesPerProducer; i++ {
		b := make([]byte, *ProducerMessageBytes)
		_, err := rand.Read(b)
		if err != nil {
			panic(err)
		}

		key := Keys[rand.Intn(len(Keys))]
		err = rdb.LPush(ctx, key, b).Err()
		if err != nil {
			panic(err)
		}
	}
}

func main() {
	// TODO: listen on ctrl+c to explicitly cancel

	flag.Parse()
	rand.Seed(*randomSeed)

	zapconfig := zap.NewDevelopmentConfig()
	zapconfig.Level = zap.NewAtomicLevelAt(*logLevel)
	zapconfig.Encoding = "json"
	zapconfig.EncoderConfig = zapcore.EncoderConfig{
		TimeKey:        "ts",
		LevelKey:       "level",
		NameKey:        "logger",
		CallerKey:      "caller",
		FunctionKey:    zapcore.OmitKey,
		MessageKey:     "msg",
		StacktraceKey:  "stacktrace",
		LineEnding:     zapcore.DefaultLineEnding,
		EncodeLevel:    zapcore.LowercaseLevelEncoder,
		EncodeTime:     zapcore.ISO8601TimeEncoder,
		EncodeDuration: zapcore.SecondsDurationEncoder, // StringDurationEncoder
		EncodeCaller:   zapcore.ShortCallerEncoder,
	}
	zapconfig.DisableCaller = true

	logger, err := zapconfig.Build()
	if err != nil {
		log.Fatalf("can't initialize zap logger: %v", err)
	}
	defer logger.Sync()

	undo := zap.ReplaceGlobals(logger)
	defer undo()

	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	consumerCtx, consumerCancel := context.WithCancel(ctx)
	defer consumerCancel()

	n := *NumKeys
	Keys = make([]string, n)
	for i := 0; i < n; i++ {
		Keys[i] = KeyPrefix + base64.StdEncoding.EncodeToString([]byte(strconv.Itoa(i)))
	}

	rdb := redis.NewClient(&redis.Options{
		Addr: "127.0.0.1:6379",
	})

	err = rdb.Ping(ctx).Err()
	if err == nil {
		log.Fatal("redis-server already running")
	}

	logger.Info("starting redis-server")
	cmd := redisServer(ctx)

	redisSystemTime0, redisUserTime0 := redisInfoCPU(ctx, rdb)

	perf := ""
	switch runtime.GOOS {
	case "linux":
		// --call-graph=dwarf
		perf = "sudo perf record -p $0 -g -F 97 -- sleep 3600"
	case "darwin":
		perf = "sample $0 3600 -file sample.out -mayDie"
	default:
		panic("unsupported OS: " + runtime.GOOS)
	}

	sample := exec.CommandContext(ctx, "bash", "-c", perf, strconv.Itoa(cmd.Process.Pid))
	if zap.L().Check(zap.DebugLevel, "") != nil {
		sample.Stdout = os.Stdout
		sample.Stderr = os.Stderr
	}
	err = sample.Start()
	if err != nil {
		panic(err)
	}

	logger.Info("starting consumers", zap.Int("num_consumers", *NumConsumers),
		zap.Duration("brpop_timeout", *ConsumerBRPopTimeout), zap.Int("brpop_num_keys", *ConsumerBRPopNumKeys),
		zap.Duration("work_duration", *ConsumerWorkDuration), zap.Float64("work_volatility", *ConsumerWorkVolatility))
	for i := 0; i < *NumConsumers; i++ {
		go redisConsumer(consumerCtx)
	}

	logger.Info("starting producers", zap.Int("num_producers", *NumProducers),
		zap.Int("messages_per_producer", *MessagesPerProducer),
		zap.Int("message_bytes", *ProducerMessageBytes))
	var wg sync.WaitGroup
	for i := 0; i < *NumProducers; i++ {
		wg.Add(1)
		go func() {
			defer wg.Done()
			redisProducer(ctx)
		}()
	}

	logger.Info("waiting on producers")
	wg.Wait()

	logger.Info("monitoring queues")
	lastLoggedPoll := time.Now()
	for {
		// TODO: backoff/timeout
		sum := int64(0)
		for _, key := range Keys {
			len := rdb.LLen(ctx, key)
			if len.Err() != nil {
				panic(len.Err())
			}
			sum += len.Val()
		}

		if sum == 0 {
			break
		}

		if time.Now().Sub(lastLoggedPoll) >= 10*time.Second {
			logger.Debug("queue poll", zap.Int64("len", sum))
			lastLoggedPoll = time.Now()
		}
		time.Sleep(2 * time.Second)
	}

	redisSystemTime1, redisUserTime1 := redisInfoCPU(ctx, rdb)
	commandStats := redisInfoCommandStats(ctx, rdb)

	logger.Info(
		"stats",
		zap.Duration("cpu_time_total", time.Duration(redisSystemTime1-redisSystemTime0)+(redisUserTime1-redisUserTime0)),
		zap.Duration("cpu_time_sys", time.Duration(redisSystemTime1-redisSystemTime0)),
		zap.Duration("cpu_time_user", time.Duration(redisUserTime1-redisUserTime0)),
		zap.Any("brpop_calls", commandStats["cmdstat_brpop_calls"]),
		zap.Any("brpop_usec", commandStats["cmdstat_brpop_usec"]),
		zap.Any("brpop_usec_per_call", commandStats["cmdstat_brpop_usec_per_call"]),
		zap.Any("lpush_calls", commandStats["cmdstat_lpush_calls"]),
		zap.Any("lpush_usec", commandStats["cmdstat_lpush_usec"]),
		zap.Any("lpush_usec_per_call", commandStats["cmdstat_lpush_usec_per_call"]),
	)

	if zap.L().Check(zap.DebugLevel, "") != nil {
		fmt.Println(rdb.Info(ctx, "stats"))
	}

	consumerCancel()

	cmd.Process.Signal(syscall.SIGINT)
	err = cmd.Wait()
	if err != nil {
		panic(err)
	}

	sample.Process.Signal(syscall.SIGINT)
	sample.Wait()
	// perf returns non-zero exit code here

	stackcollapsepl := ""
	switch runtime.GOOS {
	case "linux":
		stackcollapsepl = "sudo perf script --header -f | stackcollapse-perf.pl --kernel | flamegraph.pl --color=java --hash > flamegraph.svg"
	case "darwin":
		stackcollapsepl = "cat sample.out | stackcollapse-sample.awk | flamegraph.pl --color=java --hash > flamegraph.svg"
	default:
		panic("unsupported OS: " + runtime.GOOS)
	}

	stackcollapse := exec.CommandContext(ctx, "bash", "-c", stackcollapsepl)
	if *logLevel == zap.DebugLevel {
		stackcollapse.Stdout = os.Stdout
		stackcollapse.Stderr = os.Stderr
	}
	err = stackcollapse.Run()
	if err != nil {
		panic(err)
	}
}
