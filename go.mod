module github.com/igorwwwwwwwwwwwwwwwwwwww/redis-brpop-microbench

go 1.16

require (
	github.com/cenkalti/backoff/v4 v4.1.0
	github.com/go-redis/redis/v8 v8.8.0
	go.uber.org/multierr v1.6.0 // indirect
	go.uber.org/zap v1.16.0
)
